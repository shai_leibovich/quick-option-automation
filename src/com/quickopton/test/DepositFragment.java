package com.quickopton.test;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class DepositFragment {

	static boolean withBonus = true;
	static boolean bonusShown;
	public static void depositFragmentStage(boolean needBonus){
		withBonus = needBonus;
		toDepositFromNavigation();
		makeDeposit();
		System.out.println("Finish deposit stage");
		Functions.endStage();
	}
	
	private static void toDepositFromNavigation() {
		if (!Config.errorHappened) {
			try {
				System.out.println("Getting to deposit fragment from navigation menu.");
				Functions.buttonClickUse("com.quickoption.app:id/menu_icon");
				try {
					Thread.sleep(1000);
				} catch (Exception e) {
				}
				Functions.buttonClickUse("com.quickoption.app:id/menu_depositClickArea");
				Functions.countTime("com.quickoption.app:id/depositCreditCardNumber");
				Functions.checkPush();
				System.out.println("Entered to deposit from navigation menu succeeded.");
				Functions.endFunctions();
			} catch (Exception e) {
				System.out.println("Deposit - Failed to get to deposit from navigation menu.");
			}
		}
	}
	
	private static void toDepositFromBonus() {
		if (!Config.errorHappened) {
			try {
				System.out.println("Getting to deposit fragment from bonus menu.");
				Functions.buttonClickUse("com.quickoption.app:id/menu_icon");
				try {
					Thread.sleep(1000);
				} catch (Exception e) {
				}
				Functions.buttonClickUse("com.quickoption.app:id/start_assetBar_pickerArrow");
				Functions.countTime("com.quickoption.app:id/vipItem_icon");
				Functions.checkPush();
				Config.clickButton = Config.driver.findElement(By.name("Platinum Membership Membership"));
				Config.clickButton.click();
				Functions.countTime("com.quickoption.app:id/platinumPackage_activateBtn");
				Functions.checkPush();
				System.out.println("Entered platinum package successfully.");
				Functions.buttonClickUse("com.quickoption.app:id/platinumPackage_activateBtn");
				Functions.countTime("com.quickoption.app:id/completeDepositBtn");
				Functions.checkPush();
				System.out.println("Entered to deposit fragment from deposit succeeded.");
				System.out.println("Checking if the picker chose the right bonus.");
				Functions.checkLevel("platinum");
				Functions.endFunctions();
			} catch (Exception e) {
				System.out.println("Deposit - Failed to get to deposit from upgrade to vip.");
			}
		}
	}
	
	private static void toDepositFromQuick(){
		try {
			System.out.println("Getting to deposit fragment from quick deposit menu.");

			//TODO
			Functions.checkPush();
			Functions.endFunctions();
		}catch (Exception e) {
			System.out.println("Deposit - Failed to get to deposit from quick deposit.");
		}
	}
	
	private static void toDepositFromExpert(){
		try {
			System.out.println("Getting to deposit fragment from expert signals menu.");
			//TODO
			Functions.checkPush();
			Functions.endFunctions();
		}catch (Exception e) {
			System.out.println("Deposit - Failed to get to deposit from expert signals.");
		}
	}
	
	private static void toDepositFromFinance(){
		try {
			System.out.println("Getting to deposit fragment from finance news menu.");
			//TODO
			Functions.checkPush();
			Functions.endFunctions();
		}catch (Exception e) {
			System.out.println("Deposit - Failed to get to deposit from finance news.");
		}
	}
	
	private static void toDepositFromProfile(){
		try {
			System.out.println("Getting to deposit fragment from profile menu.");

			//TODO
			Functions.checkPush();
			Functions.endFunctions();
		}catch (Exception e) {
			System.out.println("Deposit - Failed to get to deposit from profile fragment.");
		}
	}
	
	public static void makeDeposit(){
		if(!Config.errorHappened) {
			try {
				System.out.println("Going to make a deposit with the lowest amount.");
				// Expanding deposit amount picker
				Functions.buttonClickUse("com.quickoption.app:id/deposit_chosenAmount");
				// Choosing the first deposit amount available
				List<WebElement> depsitAvailable = Config.driver.findElements(By.id("com.quickoption.app:id/depositAmountsList_chosenAmount"));
				depsitAvailable.get(0).click();
				try {
					Thread.sleep(1500);
				} catch (InterruptedException e1) {
					e1.printStackTrace();
				}
				Config.clickButton = Config.driver.findElement(By.id("com.quickoption.app:id/deposit_chosenAmount"));
				try {
					Thread.sleep(1500);
				} catch (InterruptedException e1) {
					e1.printStackTrace();
				}
				String depositAmount = Config.clickButton.getText();
				System.out.println("Going to make a deposit with " + depositAmount);
				Functions.textFieldUse("com.quickoption.app:id/depositCreditCardNumber", Config.CC_NUMBER);
				Functions.buttonClickUse("com.quickoption.app:id/creditCardExpiryMonth");


				Config.clickButton = Config.driver.findElement(By.id("com.quickoption.app:id/expiryMonthWheel"));


				int topY = Config.clickButton.getLocation().getY();
				int bottomY = topY + Config.clickButton.getSize().getHeight();
				int centerX = Config.clickButton.getLocation().getX() + (Config.clickButton.getSize().getWidth() / 2);
				Config.driver.swipe(centerX, bottomY - 100, centerX, bottomY - 15, 100);
				try {
					Thread.sleep(1500);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				Functions.buttonClickUse("com.quickoption.app:id/expiryDialog_applyBtn");
				Functions.textFieldUse("com.quickoption.app:id/depositCvv", Config.CCV);
				Config.driver.navigate().back();
				if (!withBonus) {
					System.out.println("Making deposit with no bonus.");
					Functions.buttonClickUse("com.quickoption.app:id/deposit_bonusSwitch");
					try {
						Functions.buttonClickUse("com.quickoption.app:id/deposit_bonusAmountText");
						bonusShown = true;
					} catch (Exception e) {
						bonusShown = false;
					}
					if (!bonusShown) {
						System.out.println("Setting no bonus complete");
					} else {
						System.out.println("Setting no bonus failed.");
						Functions.addError("Deposit fragment - cannot switch from bonus to none bonus");
					}
				}
				Functions.buttonClickUse("com.quickoption.app:id/completeDepositBtn");
				Functions.countTime("com.quickoption.app:id/confirmAdressDialog_confirmBtn");
				finishDeposit();
			} catch (Exception e) {
				System.out.println("Deposit - Failed to get to make a deposit.");
			}
		}
	}
		
	public static void finishDeposit(){
		if(!Config.errorHappened) {
			try {
				System.out.println("Complete funding popup.");
				Functions.buttonClickUse("com.quickoption.app:id/confirmAdressDialog_confirmBtn");
				Functions.endFunctions();
				try {
					Thread.sleep(5000);
					Config.clickButton = Config.driver.findElement(By.id("com.quickoption.app:id/tvHeaderMessage"));
					String errorText = Config.clickButton.getText();

//			switch (errorText){
//				case "Please try another card or contact your bank":

//				case ""
//			}

					System.out.println("Error message:" + errorText);
					Functions.buttonClickUse("com.quickoption.app:id/headerMessage_closeBtn");
					DepositFragment.makeDeposit();
				} catch (Exception e) {
					System.out.println("Deposit succeeded, continue to successful deposit fragment");
					Functions.countTime("com.quickoption.app:id/deposit_completed_btn");
					Functions.endFunctions();
					confirmDeposit();
				}
			} catch (Exception e) {
				System.out.println("Deposit - Failed to finish deposit pop-up.");
			}
		}
	}

	private static void confirmDeposit(){
		if(!Config.errorHappened) {
			try {
				System.out.println("Entered confirm deposit fragment, continue to signature");
				Functions.buttonClickUse("com.quickoption.app:id/deposit_completed_btn");
				Functions.countTime("com.quickoption.app:id/deposit_approval_clear_signature_btn");
				Functions.endFunctions();
				signatureFragment();
			} catch (Exception e) {
				System.out.println("Deposit - Failed to finish confirm deposit fragment.");
			}
		}
	}

	public static void signatureFragment() {
		if (!Config.errorHappened) {
			try {
				System.out.println("Entered signature fragment.");
				System.out.println("Entering signature");
				Config.clickButton = Config.driver.findElement(By.id("com.quickoption.app:id/deposit_approval_clear_signature_btn"));
				int topY = Config.clickButton.getLocation().getY();
				int bottomY = topY + Config.clickButton.getSize().getHeight();
				int centerX = Config.clickButton.getLocation().getX() + (Config.clickButton.getSize().getWidth() / 2);
				Config.driver.swipe(centerX, bottomY + 100, centerX, bottomY +400, 1000);
				System.out.println("Clicking confirm to finish.");
				Functions.buttonClickUse("com.quickoption.app:id/deposit_approval_confirm_btn");
				if (SignupFragments.onSignup){
					Functions.countTime("com.quickoption.app:id/verificationProcessScanBtn");
					System.out.println("Finish deposit from sign-up");
				} else {
					Functions.countTime("com.quickoption.app:id/investPanel_investBox");
					System.out.println("Finish testing deposit.");
				}
				Functions.checkPush();
				Functions.endFunctions();
			} catch (Exception e) {
				System.out.println("Deposit - Failed to finish signature fragment.");
			}
		}
	}
	
}
