package com.quickopton.test;

import org.openqa.selenium.By;
import java.util.Random;
import io.appium.java_client.TouchAction;

public class SignupFragments{

	static boolean finishCheck = false;
	static boolean triedPincode = false;
	static boolean withDeposit;
	static boolean withCompliance;
	static boolean withPhone;
	static boolean withFNS;
	static boolean doRegister;
	static boolean onSignup = false;
	private static final String EMAIL_DOMAIN = "@mail.ru";
	private static Random _rnd = new Random();

	public static void singUpStage(boolean deposit, boolean compliance , boolean phone , boolean fns, boolean register){
		withDeposit = deposit;
		withCompliance = compliance;
		withPhone = phone;
		withFNS = fns;
		doRegister = register;
		startSignupFragment();
		if (doRegister) {
			onSignup = true;
			meetPA();
			euroRegulationFragment();
			fnsFragment();
			signupDepositFragment();
		}
		System.out.println("Finish signup stage");
		onSignup = false;
		Functions.endStage();
	}
	
	public static void startSignupFragment(){
		if (!Config.errorHappened){
			try {
				if (doRegister) {
					System.out.println("Enter start page. Going to continue to Sign-up page");
					Functions.countTime("com.quickoption.app:id/signUpButton");
					Functions.buttonClickUse("com.quickoption.app:id/signUpButton");
					System.out.println("Finish with start fragment. Continue to signup page");
					Functions.countTime("com.quickoption.app:id/signUpComplete_firstNameTv");
					Functions.endFunctions();
					signupFragment();
				} else {
					System.out.println("Enter start page. Going to continue to login page");
					Functions.countTime("com.quickoption.app:id/signUpButton");
					Functions.buttonClickUse("com.quickoption.app:id/loginButton");
					System.out.println("Finish with start fragment. Continue to signup page");
					Functions.countTime("com.quickoption.app:id/loginBtn");
					Functions.endFunctions();
					loginFragment();
				}
			} catch (Exception e) {
				Functions.addError("Signup - Failed in finishing startFragment.");
			}
		}
	}

	public static void signupFragment() {
		if (!Config.errorHappened) {
			try {
				System.out.printf("Enter signup page. Going to signup for user %s with email %s\n", Config.LAST_NAME, Config.USER_EMAIL);
				// Set first name as firstName
				Functions.textFieldUse("com.quickoption.app:id/signUpComplete_firstNameTv", Config.FIRST_NAME);
				//		Functions.checkNextFunctions("first name");
				System.out.println("Entering first name completed");
				// Set last name as lastName
				Functions.textFieldUse("com.quickoption.app:id/signUpComplete_lastNameTv", Config.LAST_NAME);
				//		Functions.checkNextFunctions("last name");
				System.out.println("Entering last name completed");
				//set email as email
				Functions.textFieldUse("com.quickoption.app:id/signUpComplete_userEmailEt",generateEmail());
				//		Functions.checkNextFunctions("email");
				System.out.println("Entering email completed");
				//set password and confirm password as userPassword
				Functions.textFieldUse("com.quickoption.app:id/signUpComplete_userPasswordEt", Config.USER_PASSWORD);
				System.out.println("Entering password completed");
				//		Functions.checkNextFunctions("password");
				Functions.textFieldUse("com.quickoption.app:id/signUpComplete_confirmPasswordEt", Config.USER_PASSWORD);
				System.out.println("Entering confirm password completed");
				//		Functions.checkNextFunctions("password confirm");
				//set country as country
				Functions.textFieldUse("com.quickoption.app:id/signUpComplete_countryTv", Config.COUNTRY);
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				Config.clickButton = Config.driver.findElement(By.id("com.quickoption.app:id/signUpComplete_countryTv"));
				int topY = Config.clickButton.getLocation().getY();
				int centerX = Config.clickButton.getLocation().getX() + (Config.clickButton.getSize().getWidth() / 2);
				new TouchAction(Config.driver).tap(centerX, topY - 50).release().perform();
				//set currency to US-Dollar

				Config.clickButton = Config.driver.findElement(By.name("US Dollar"));
				Config.clickButton.click();
				System.out.println("Entering country completed");
				//set check box as true
				Functions.buttonClickUse("com.quickoption.app:id/signUp_checkbox");
				System.out.println("Setting check-box as checked");

				//Finishing sign-up
				Functions.buttonClickUse("com.quickoption.app:id/signUpComplete_Tv");
				System.out.println("Signup have finished");
				Functions.endFunctions();
				Functions.countTime("com.quickoption.app:id/phoneVerifyCountryTv");
				phoneVerificationFragment();
			} catch (Exception e) {
				Functions.addError("Signup - Failed in registration");
			}
		}
	}
		
	public static void phoneVerificationFragment(){
		if(!Config.errorHappened) {
			try {
				if(withPhone) {
					System.out.println("Entering phone verification");
					Functions.textFieldUse("com.quickoption.app:id/phoneVerifyCountryTv", Config.TESTER_COUNTRY);
					System.out.println("Entering tester country completed");
					Config.driver.sendKeyEvent(66);

					String numPadButtons = "com.quickoption.app:id/numPad_num";
					//continue by clicking the pin-code
					for (int counter = 0; counter < Config.TESTER_PHONE.length(); counter++) {
						Functions.buttonClickUse(numPadButtons + Config.TESTER_PHONE.charAt(counter) + "_tv");
					}
					Functions.buttonClickUse("com.quickoption.app:id/numPad_doneBtnText");

					System.out.println("Entering tester phone completed");
					System.out.println("Finishing phone verification fragment");
					Functions.endFunctions();

					try {
						Thread.sleep(3000);
						Functions.countTime("com.quickoption.app:id/keypad1");
						System.out.println("The phone don't have SIM and the user have to add manually the pin-code.");
						phoneVerificationPincodeFragment();
					} catch (Exception e) {
						System.out.println("The phone have SIM and continue to meetPA fragment");
						Functions.countTime("com.quickoption.app:id/continueButton");
					}
				} else {
					System.out.println("Entering phone verification");
					System.out.println("Clicking skip button.");
					Functions.buttonClickUse("com.quickoption.app:id/titleBar_skipBtn");
					Functions.countTime("com.quickoption.app:id/continueButton");
					Functions.endFunctions();
				}
			} catch (Exception e) {
				Functions.addError("Signup - Failed in finishing phoneVerification.");
			}
		}
	}
	
	public static void phoneVerificationPincodeFragment(){
		if(!Config.errorHappened) {
			try {
				while (!finishCheck) {
					if (!triedPincode) {
						Functions.checkPush();
						System.out.println("Entering phone pin-code verification");
					}
					System.out.println("Enter the pin-code that you have received");
					Functions.getPincodePopup();
					String numPadButtons = "com.quickoption.app:id/keypad";
					//continue by clicking the pin-code
					for (int counter = 0; counter < Config.pinCode.length(); counter++) {
						Functions.buttonClickUse(numPadButtons + Config.pinCode.charAt(counter));
					}
					triedPincode = true;
					Functions.buttonClickUse("com.quickoption.app:id/completePhoneVerification");
					try {
						Thread.sleep(2000);
						Config.clickButton = Config.driver.findElement(By.name("Invalid PIN code."));
						System.out.println("Wrong pin-code. Please enter again.");
						Config.pinCode = null;
					} catch (Exception e) {
						Functions.countTime("com.quickoption.app:id/continueButton");
						System.out.println("Finish phone verification pin-code fragment");
						finishCheck = true;
						Functions.endFunctions();
					}
				}
			} catch (Exception e) {
				Functions.addError("Signup - Failed in finishing phoneVerification pin-code");
			}
		}
	}
	
	public static void meetPA() {
		if (!Config.errorHappened) {
			try {
				System.out.println("Entering meet your PA.");
				Functions.buttonClickUse("com.quickoption.app:id/continueButton");
				System.out.println("Finish meet your PA fragment.");
				Functions.endFunctions();
				Functions.countTime("com.quickoption.app:id/regulationInfo_btn");
			} catch (Exception e) {
				Functions.addError("Signup - Failed in finishing meet personal assistant");
			}
		}
	}
	
	public static void euroRegulationFragment() {
		if(!Config.errorHappened) {
			try {
				Functions.checkPush();
				System.out.println("Entering European Regulation fragment.");
				Functions.buttonClickUse("com.quickoption.app:id/regulationInfo_btn");
				System.out.println("Finish European Regulation fragment.");
				Functions.endFunctions();
				Functions.countTime("com.quickoption.app:id/layout_send_btn");
			} catch (Exception e) {
				Functions.addError("Signup - Failed in finishing Euro regulation fragment");
			}
		}
	}
	
	public static void fnsFragment() {
		if (!Config.errorHappened) {
			try {
				Functions.checkPush();
				System.out.println("Entering FNS fragment.");
				boolean finishFns = false;
				int i = 1;
				while (!finishFns) {
					finishFns = Functions.checkAnswer(i,withFNS);
					i++;
				}
				Functions.endFunctions();
				Functions.countTime("com.quickoption.app:id/completeDepositBtn");
			} catch (Exception e) {
				Functions.addError("Signup - Failed in finishing FNS");
			}
		}
	}
	
	public static void signupDepositFragment(){
		if(!Config.errorHappened) {
			try {
				Functions.checkPush();
				System.out.println("Entering deposit on signup fragment.");
				if (!withDeposit) {
					System.out.println("Skipping deposit fragment");
					Functions.buttonClickUse("com.quickoption.app:id/titleBar_skipBtn");
					Functions.buttonClickUse("com.quickoption.app:id/skipFundAccount_confirmBtn");
					System.out.println("Finish deposit on signup fragment.");
				} else {
					System.out.println("Finishing deposit");
					DepositFragment.makeDeposit();
					if (withFNS){
						System.out.println("Finished deposit at sign-up and FNS, continue to verification process");
						verificationProcessSign();
						System.out.println("Finishing verification process after successful deposit");
					} else {
						System.out.println("Finished deposit at signup without fns, complete fns now.");
						withFNS = true;
						fnsFragment();
						System.out.println("Finishing fns after deposit");
					}
					Functions.checkPush();
					Functions.endFunctions();
					try {
						Functions.countTime("com.quickoption.app:id/investPanel_investBox");
						System.out.println("Entering trade screen after signup fragment.");
						Functions.endFunctions();
					} catch (Exception e) {
						System.out.println("Cheking if the date is sunday.");
						Functions.countTime("com.quickoption.app:id/marketOpenIn_fundBtn");
						System.out.println("It is sunday.");
						Functions.endFunctions();
					}
				}
			} catch (Exception e) {
				Functions.addError("Signup - Failed in finishing deposit fragment");
			}
		}
	}

	public static void loginFragment(){
		if(!Config.errorHappened){
			try{
				Functions.checkPush();
				System.out.println("Entered login page successfully.");
				Functions.textFieldUse("com.quickoption.app:id/login_userEmailEt",Config.USER_EMAIL);
				if (Config.changedPassword) {
					Functions.textFieldUse("com.quickoption.app:id/loginUserPasswordEt",Config.USER_NEW_PASSWORD);
				} else {
					Functions.textFieldUse("com.quickoption.app:id/loginUserPasswordEt",Config.USER_PASSWORD);
				}
				Config.driver.navigate().back();
				Functions.buttonClickUse("com.quickoption.app:id/loginBtn");
				try {
					Functions.countTime("com.quickoption.app:id/investPanel_investBox");
					System.out.println("Entering trade screen after login.");
					Functions.endFunctions();
				} catch (Exception e) {
					System.out.println("Cheking if the date is sunday.");
					Functions.countTime("com.quickoption.app:id/marketOpenIn_fundBtn");
					System.out.println("It is sunday.");
					Functions.endFunctions();
				}
			} catch (Exception e){
				Functions.addError("Signup - Failing in login");
			}
		}
	}

	public static void verificationProcessSign(){
		if (!Config.errorHappened){
			try {
				System.out.println("Entering verification process from sign-up");
				System.out.println("Skipping verification process");
				Functions.buttonClickUse("com.quickoption.app:id/titleBar_skipBtn");
				Functions.countTime("com.quickoption.app:id/skipVerification_confirmBtn");
				Functions.buttonClickUse("com.quickoption.app:id/skipVerification_confirmBtn");
				try {
					Functions.countTime("com.quickoption.app:id/investPanel_investBox");
					System.out.println("Entering trade screen after signup fragment.");
					Functions.endFunctions();
				} catch (Exception e) {
					System.out.println("Cheking if the date is sunday.");
					Functions.countTime("com.quickoption.app:id/marketOpenIn_fundBtn");
					System.out.println("It is sunday.");
					Functions.endFunctions();
				}
			} catch (Exception e){
				Functions.addError("Signup - Failed in skipping verification process");
			}
		}
	}

	public static String generateEmail() {  // ++++++++++++++++++++++++++
		Config.USER_EMAIL = Config.FIRST_NAME + _rnd.nextInt(999999) + EMAIL_DOMAIN;
		return Config.USER_EMAIL;
	}
}
