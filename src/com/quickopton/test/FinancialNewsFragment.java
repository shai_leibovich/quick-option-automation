package com.quickopton.test;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class FinancialNewsFragment {
	static boolean freeTrial = false;
	static boolean financeAvailable = false;
	static String currentTrade;
	static boolean returnedFromTrade = false;
	
	public static void financialNewsFragmentStage(){
		getToFinancial();
		financeNewsFragmentReadButton();
		financeNewsTradeButton();
		System.out.println("Finish financial news stage");
		Functions.endStage();
	}
	
	private static void getToFinancial(){
		if(!Config.errorHappened) {
			try {
				returnedFromTrade = true;
				System.out.println("Getting into financial news fragment.");
				Functions.buttonClickUse("com.quickoption.app:id/menu_icon");
				Config.clickButton = Config.driver.findElement(By.name("Financial News"));
				Config.clickButton.click();
				Functions.countTime("com.quickoption.app:id/menu_icon");
				Functions.checkPush();
				Functions.endFunctions();
				try {
					Config.clickButton = Config.driver.findElement(By.id("com.quickoption.app:id/accessToMembers_tv2"));
					freeTrial = true;
				} catch (Exception e) {
					freeTrial = false;
				}

				if (freeTrial) {
					System.out.println("User have free trial page, checking free trial page.");
					freeTrialFragment();
				}
			} catch (Exception e) {
				Functions.addError("Financial news - Failed in entering financial news.");
			}
		}
	}
	
	private static void freeTrialFragment(){
		if(!Config.errorHappened) {
			try {
				System.out.println("Checking upgrade button.");
				Functions.buttonClickUse("com.quickoption.app:id/animBtn2");
				Functions.countTime("com.quickoption.app:id/completeDepositBtn");
				Functions.checkPush();
				Functions.checkLevel("silver");
				System.out.println("Returning to finance news fragment.");
				Functions.buttonClickUse("com.quickoption.app:id/menu_icon");
				Config.clickButton = Config.driver.findElement(By.name("Financial News"));
				Config.clickButton.click();
				Functions.countTime("com.quickoption.app:id/animBtn2");
				Functions.checkPush();
				System.out.println("Returning to finance news fragment completed");
				System.out.println("Clicking on free trial button");
				Functions.buttonClickUse("com.quickoption.app:id/accessToMembers_tv2");
				Functions.countTime("com.quickoption.app:id/forexDate_dayOfMonth");
				System.out.println("Free trial test completed.");
				Functions.endFunctions();
			} catch (Exception e) {
				Functions.addError("Financial news - Failed to finish free trial.");
			}
		}
	}

	private static void financeNewsCheck(){
		if(!Config.errorHappened) {
			try {
				System.out.println("Start swiping right 7 times.");
				for (int i = 0; i < 7; i++) {
					Config.driver.swipe(925, 925, 125, 925, 100);
					try {
						Thread.sleep(1500);
					} catch (InterruptedException e) {
					}
				}
				System.out.println("Swiping 7 times right succeeded. Trying to swipe left 7 times.");
				for (int i = 0; i < 7; i++) {
					Config.driver.swipe(125, 925, 925, 925, 100);
					try {
						Thread.sleep(1500);
					} catch (InterruptedException e) {
					}
					System.out.println("Swiping 7 times left succeeded.");
				}
				int counter = 0;
				while (!financeAvailable || counter != 7) {
					try {
						Config.clickButton = Config.driver.findElement(By.id("com.quickoption.app:id/newsList_readBtn"));
						financeAvailable = true;
					} catch (Exception e) {
						System.out.println("Failed to found finance news available, trying to swipe right");
						Config.driver.swipe(925, 925, 125, 925, 100);
					}
				}
				if (!financeAvailable) {
					System.out.println("No finance available, finish testing here.");
					Functions.addError("Finance news - Couldn't found any finance news available");
					Functions.returnTradeFragment();
				}
				returnedFromTrade = false;
			} catch (Exception e) {
				Functions.addError("Financial news - Failed in checking available news.");
			}
		}
	}
	
	private static void financeNewsFragmentReadButton(){
		List<WebElement> readButton = Config.driver.findElements(By.id("com.quickoption.app:id/newsList_readBtn"));
		List<WebElement> titleList = Config.driver.findElements(By.id("com.quickoption.app:id/newsItem_eventTv"));
		List<WebElement> currentTradeList;
		boolean tradeAvailable = true;
		if(!Config.errorHappened) {
			try {
				if (returnedFromTrade) {
					financeNewsCheck();
				}

				System.out.println("Found " + readButton.size() + " available news.");
				System.out.println("Click on first news read button.");
				readButton.get(0).click();
				Functions.countTime("com.quickoption.app:id/headerTitle");
				Config.clickButton = Config.driver.findElement(By.id("com.quickoption.app:id/newsItem_eventTv"));
				if (Config.clickButton.getText() == titleList.get(0).getText()) {
					System.out.println("Entered to the correct page.");
				} else {
					Functions.addError("Finance news - clicking read button get the wrong page.");
					System.out.println("Entered to the wrong page. Continue");
				}
				System.out.println("Checking back button");
				Functions.buttonClickUse("com.quickoption.app:id/backButtonIcon");
				Functions.countTime("com.quickoption.app:id/headerTitle");
				Functions.checkPush();
				System.out.println("Back button succeeded. Returning to first finance news");
				readButton.get(0).click();
				Functions.countTime("com.quickoption.app:id/headerTitle");
				System.out.println("Clicking on trade button.");
				currentTradeList = Config.driver.findElements(By.id("com.quickoption.app:id/current_asset"));
				currentTrade = currentTradeList.get(0).getText();
				System.out.println("Clicking on trade screen on first trade option.");

				if (currentTradeList.size() == 0) {
					System.out.println("No trade option.");
					tradeAvailable = false;
					Functions.addError("Finance news - Failed in finding any trade available");
					System.out.println("Clicking back button");
					Functions.buttonClickUse("com.quickoption.app:id/backButtonIcon");
					Functions.countTime("com.quickoption.app:id/tradeBtn_new2");
					Functions.checkPush();
					System.out.println("Returned to the asset read fragment. Clicking back again.");
					Functions.buttonClickUse("com.quickoption.app:id/backButtonIcon");
					Functions.countTime("com.quickoption.app:id/menu_icon");
					Functions.checkPush();
					System.out.println("Returned to finance news fragment completed.");
				}
				if (tradeAvailable) {
					Functions.buttonClickUse("com.quickoption.app:id/tradeBtn_new2");
					Functions.countTime("com.quickoption.app:id/start_assetBar_currentAssetTv");
					Functions.checkPush();
					Config.clickButton = Config.driver.findElement(By.id("com.quickoption.app:id/start_assetBar_currentAssetTv"));
					if (Config.clickButton.getText().equals(currentTrade)) {
						System.out.println("Open the same asset that was clicked: " + currentTrade);
					}
				}
				System.out.println("Returning to finance news fragment");
				getToFinancial();
			} catch (Exception e) {
				Functions.addError("Financial news - Failed in checking trade button functionality.");
			}
		}
	}

	private static void financeNewsTradeButton(){
		String titleOfTrade;
		if(!Config.errorHappened) {
			try {
				if (returnedFromTrade) {
					financeNewsCheck();
				}
				List<WebElement> tradeButtons = Config.driver.findElements(By.id("com.quickoption.app:id/newsList_tradeBtn"));
				List<WebElement> titleList = Config.driver.findElements(By.id("com.quickoption.app:id/newsItem_eventTv"));
				System.out.println("Clicking on trade button of first asset.");
				tradeButtons.get(0).click();
				Functions.countTime("com.quickoption.app:id/backButtonIcon");
				Functions.checkPush();
				Config.clickButton = Config.driver.findElement(By.id("com.quickoption.app:id/newsItem_eventTv"));
				titleOfTrade = Config.clickButton.getText();
				if (titleOfTrade != titleList.get(0).getText()) {
					System.out.println("Trade button get into the wrong asset");
					Functions.addError("Finance news - Trade button take to the wrong asset");
				}
			} catch (Exception e) {
				Functions.addError("Financial news - Failed in checking trade button functionality.");
			}
		}
	}
}
