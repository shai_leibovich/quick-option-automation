package com.quickopton.test;

import org.openqa.selenium.By;

public class AccountSetting {
	
	public static void accountSettingStage(){
		getToAccountSetting();
		accountInformationFragment();
		personalInformationFragment();
		changePasswordFragment();
		logOutFunctionality();
		Functions.returnTradeFragment();
		System.out.println("Finish account setting stage");
		Functions.endStage();
	}
	
	public static void getToAccountSetting(){
		if(!Config.errorHappened) {
			try {
				System.out.println("Entering navigation menu to enter account setting");
				Functions.buttonClickUse("com.quickoption.app:id/menu_icon");
				Config.clickButton = Config.driver.findElement(By.name("Account Settings"));
				Config.clickButton.click();
				System.out.println("Starting counting time for loading account setting.");
				Functions.countTime("com.quickoption.app:id/accWithdrawBtn");
				Functions.checkPush();
				Functions.endFunctions();
			} catch (Exception e) {
				Functions.addError("Account setting - Failed in getting to account setting from trade fragment.");
			}
		}
	}
	
	public static void getToAccountTab(){
		if(!Config.errorHappened) {
			try {
				Config.clickButton = Config.driver.findElement(By.name("Account"));
				Config.clickButton.click();
				System.out.println("Entering Account tab at account setting");
			} catch (Exception e) {
				Functions.addError("Account setting - Failed in getting to account tab.");
			}
		}
	}

	public static void getToBankingTab(){
		if(!Config.errorHappened) {
			try {
				Config.clickButton = Config.driver.findElement(By.name("Banking"));
				Config.clickButton.click();
				System.out.println("Entering banking tab at account setting");
			} catch (Exception e) {
				Functions.addError("Account setting - Failed in getting to bank tab.");
			}
		}
	}
	
	public static void getToVerificationTab(){
		if(!Config.errorHappened) {
			try {
				Config.clickButton = Config.driver.findElement(By.name("Verification"));
				Config.clickButton.click();
				System.out.println("Entering verification tab at account setting");
			} catch (Exception e) {
				Functions.addError("Account setting - Failed in getting to verification tab.");
			}
		}
	}
	
	public static void getToAccountInformation(){
		if(!Config.errorHappened) {
			try {
				getToAccountTab();
				Functions.buttonClickUse("com.quickoption.app:id/accSettings_accountInfoBtn");
				System.out.println("Entering account information fragment");
				Functions.endFunctions();
			} catch (Exception e) {
				Functions.addError("Account setting - Failed in getting to account information fragment.");
			}
		}
	}

	public static void getToChangePasswordFragment(){
		if(!Config.errorHappened) {
			try {
				getToAccountTab();
				Functions.buttonClickUse("com.quickoption.app:id/accSettings_changePasswordBtn");
				System.out.println("Entering change password fragment");
				Functions.endFunctions();
			} catch (Exception e) {
				Functions.addError("Account setting - Failed in getting to change password fragment.");
			}
		}
	}
	
	public static void getToPersonalInformationFragment(){
		if(!Config.errorHappened) {
			try {
				getToAccountTab();
				Functions.buttonClickUse("com.quickoption.app:id/accSettings_personalInfoBtn");
				System.out.println("Entering personal Information fragment");
				Functions.endFunctions();
			} catch (Exception e) {
				Functions.addError("Account setting - Failed in getting to personal information fragment.");
			}
		}
	}
	
	public static void backButton(){
		System.out.println("Clicking back button");
		Functions.buttonClickUse("com.quickoption.app:id/backButtonIcon");
	}
	
	public static void accountInformationFragment(){
		if(!Config.errorHappened) {
			try {
				getToAccountInformation();
				Functions.countTime("com.quickoption.app:id/privacy_toggleImg");
				System.out.println("Enter account information fragment successfully");
				Functions.checkPush();
				System.out.println("Exit from account information fragment");
				Functions.endFunctions();
				backButton();
				Functions.countTime("com.quickoption.app:id/accSettings_personalInfoBtn");
				Functions.checkPush();
			} catch (Exception e) {
				Functions.addError("Account setting - Failed in completing account information fragment.");
			}
		}
	}

	public static void personalInformationFragment(){
		if(!Config.errorHappened) {
			try {
				getToPersonalInformationFragment();
				Functions.countTime("com.quickoption.app:id/personalInfo_birthdate_value");
				System.out.println("Enter personal information fragment successfully");
				Functions.checkPush();
				System.out.println("Exit from personal information fragment");
				Functions.endFunctions();
				backButton();
				Functions.countTime("com.quickoption.app:id/accSettings_personalInfoBtn");
				Functions.checkPush();
			} catch (Exception e) {
				Functions.addError("Account setting - Failed in completing personal information fragment.");
			}
		}
	}
	
	public static void changePasswordFragment(){
		if(!Config.errorHappened) {
			try {
				getToChangePasswordFragment();
				Functions.countTime("com.quickoption.app:id/changePassword_oldPassword");
				System.out.println("Enter change password fragment successfully");
				System.out.println("Testing back button");
				backButton();
				Functions.countTime("com.quickoption.app:id/accSettings_logoutText");
				Functions.checkPush();
				System.out.println("Back successfully , returning to change password fragment.");
				getToChangePasswordFragment();
				Functions.countTime("com.quickoption.app:id/changePassword_oldPassword");
				Functions.checkPush();
				System.out.println("Entering current password: " + Config.USER_PASSWORD);
				Functions.textFieldUse("com.quickoption.app:id/changePassword_oldPassword", Config.USER_PASSWORD);
				System.out.println("Entering new password: " + Config.USER_NEW_PASSWORD);
				Functions.textFieldUse("com.quickoption.app:id/changePassword_newPassword", Config.USER_NEW_PASSWORD);
				System.out.println("Entering confirm new password: " + Config.USER_NEW_PASSWORD);
				Functions.textFieldUse("com.quickoption.app:id/changePassword_confirmPassword", Config.USER_NEW_PASSWORD);
				Config.driver.navigate().back();
				Functions.buttonClickUse("com.quickoption.app:id/changePassword_saveChangesBtn");
				System.out.println("Finishing change password fragment.");
				Functions.countTime("com.quickoption.app:id/accSettings_logoutText");
				Functions.checkPush();
				Functions.endFunctions();
				Config.changedPassword = true;
			} catch (Exception e) {
				Functions.addError("Account setting - Failed in completing change password fragment.");
			}
		}
	}
	
	public static void logOutFunctionality(){
		try {
			System.out.println("Checking logout functionality");
			getToAccountTab();
			System.out.println("Clicking Logout button");
			Functions.buttonClickUse("com.quickoption.app:id/accSettings_logoutText");
			Functions.countTime("com.quickoption.app:id/loginButton");
			System.out.println("Logout successfully, Going to log-in fragment.");
			Functions.buttonClickUse("com.quickoption.app:id/loginButton");
			Functions.countTime("com.quickoption.app:id/loginBtn");
			Functions.checkPush();
			System.out.println("Entering email address");
			Functions.textFieldUse("com.quickoption.app:id/login_userEmailEt", Config.USER_EMAIL);
			if (Config.changedPassword) {   // Checking if password have changed
				System.out.println("Entering new password after changed");
				Functions.textFieldUse("com.quickoption.app:id/loginUserPasswordEt", Config.USER_NEW_PASSWORD);
			} else {
				System.out.println("Entering password after changed");
				Functions.textFieldUse("com.quickoption.app:id/loginUserPasswordEt", Config.USER_PASSWORD);
			}
			Config.driver.navigate().back();
			Functions.buttonClickUse("com.quickoption.app:id/loginBtn");
			Functions.countTime("com.quickoption.app:id/investPanel_investBox");
			System.out.println("Logged in succesfully.");
			Functions.checkPush();
			Functions.endFunctions();
		} catch (Exception e){
			Functions.addError("Account setting - Failed in completing log out function.");
		}
		
		
	}
}
