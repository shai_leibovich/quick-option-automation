package com.quickopton.test;

import java.util.Scanner;
import org.openqa.selenium.WebElement;
import io.appium.java_client.android.AndroidDriver;

public class Config {
	public static AndroidDriver driver;

	/**
	TIMER COUNTING FIELDS
	**/
	public static long startTime;
	public static long endTime;
	public static long totalTime;
	public static double totalTimeSeconds;

	/**
	 CREATING USER FIELDS
	 **/
	public static final String FIRST_NAME = "Test";
	public static final String LAST_NAME = "Automation";
	public static final String USER_PASSWORD = "123456";
	public static String USER_EMAIL;
	public static final String COUNTRY = "Germany";
	public static final String TESTER_PHONE = "0544480403";
	public static final String TESTER_COUNTRY = "Israel";
	public static final String USER_NEW_PASSWORD = "1234567";
	// Device user fields
//	public static final String DEVICE_NAME = "05cd7ba3004d1ba5";
	public static final String DEVICE_NAME = "93a1f5a4";
//	public static final String DEVICE_NAME = "emulaotr-5554";
//	public static final String PLATFORM_NAME = "Android Emulator";
	public static final String PLATFORM_NAME = "Android";
	
	public static boolean changedPassword = false;
	public static String error[] = new String[100];
	public static int errorCounter = 0;
	public static boolean errorHappened = false;
	public static String pinCode;
	public static Scanner input = new Scanner(System.in);
	public static WebElement clickButton;
	public static WebElement textField;

	/**
	  DEPOSIT DETAILS
	 **/

	public static final String CC_NUMBER = "4000020951595032";
	public static final String CCV = "115";
	public static final boolean bonusUse = true;
	
}
