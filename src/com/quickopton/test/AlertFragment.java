package com.quickopton.test;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class AlertFragment {
	
	public static void alertFragmentStage(){
		getToAlert();
		getToCreateAlert();
		addAlert();
		editAlert();
		Functions.returnTradeFragment();
		System.out.println("Finish testing alert fragment.");
	}
	
	private static void getToAlert(){
		if(!Config.errorHappened) {
			try {
				System.out.println("Entering navigation menu to enter alerts");
				Functions.buttonClickUse("com.quickoption.app:id/menu_icon");
				Config.clickButton = Config.driver.findElement(By.name("Asset Alert"));
				Config.clickButton.click();
				System.out.println("Starting counting time for loading Alert fragment.");
				Functions.countTime("com.quickoption.app:id/alert_addAlertBtn");
				Functions.checkPush();
				Functions.endFunctions();
			} catch (Exception e) {
				System.out.println("Alert fragment - Failed in getting to alert fragment from navigation menu.");
			}
		}
	}
	
	private static void addAlert(){
		if(!Config.errorHappened) {
			try {
				System.out.println("Testing asset picker");
				Functions.buttonClickUse("com.quickoption.app:id/pickerItemTv");
				Config.clickButton = Config.driver.findElement(By.name("FACEBOOK"));
				Config.clickButton.click();
				Config.clickButton = Config.driver.findElement(By.id("com.quickoption.app:id/alert_wheel_1"));
				int topY = Config.clickButton.getLocation().getY();
				int bottomY = topY + Config.clickButton.getSize().getHeight();
				int centerX = Config.clickButton.getLocation().getX() + (Config.clickButton.getSize().getWidth() / 2);
				Config.driver.swipe(centerX, bottomY - 100, centerX, bottomY - 15, 100);
				try {
					Thread.sleep(1500);
				} catch (InterruptedException e) {
				}
				System.out.println("Changed goal rate");
				Functions.buttonClickUse("com.quickoption.app:id/submit_saveChangeBtn");
				Functions.countTime("com.quickoption.app:id/alertCreated_approveBtn");
				System.out.println("Adding another alert");
				Functions.buttonClickUse("com.quickoption.app:id/alertCreated_approveBtn");
				Functions.buttonClickUse("com.quickoption.app:id/submit_saveChangeBtn");
				Functions.countTime("com.quickoption.app:id/alertCreated_approveBtn");
				Functions.buttonClickUse("com.quickoption.app:id/alertCreated_cancelBtn");
				Functions.countTime("com.quickoption.app:id/alert_addAlertBtn");
				Functions.checkPush();
				System.out.println("Adding alert test completed successfuly");
				Functions.endFunctions();
			} catch (Exception e) {
				System.out.println("Alert fragment - Failed in adding alert.");
			}
		}
	}
	
	private static void editAlert(){
		if(!Config.errorHappened) {
			try {
				System.out.println("Edit alert test");
				try {
					List<WebElement> alerts = Config.driver.findElements(By.id("com.quickoption.app:id/openAlertsStrikeTv"));
					String startGoal = alerts.get(0).getText();
					alerts.get(0).click();
					Functions.countTime("com.quickoption.app:id/submit_saveChangeBtn");
					Config.clickButton = Config.driver.findElement(By.id("com.quickoption.app:id/alert_wheel_1"));
					int topY = Config.clickButton.getLocation().getY();
					int bottomY = topY + Config.clickButton.getSize().getHeight();
					int centerX = Config.clickButton.getLocation().getX() + (Config.clickButton.getSize().getWidth() / 2);
					Config.driver.swipe(centerX, bottomY - 100, centerX, bottomY - 15, 100);
					try {
						Thread.sleep(1500);
					} catch (InterruptedException e) {
					}
					System.out.println("Changed goal rate");
					Functions.buttonClickUse("com.quickoption.app:id/submit_saveChangeBtn");
					Functions.countTime("com.quickoption.app:id/removeAlertBtn");
					Functions.checkPush();
					alerts = Config.driver.findElements(By.id("com.quickoption.app:id/openAlertsStrikeTv"));
					String changedGoal = alerts.get(0).getText();
					if (startGoal != changedGoal) {
						System.out.println("Change alert goal succeeded.");
					} else {
						System.out.println("Change alert goal faild.");
					}

				} catch (Exception e) {
					System.out.println("No alert added");
				}
				Functions.endFunctions();
			} catch (Exception e) {
				System.out.println("Alert fragment - Failed in edit an alert.");
			}
		}
	}
	
	private static void getToCreateAlert() {
		if (!Config.errorHappened) {
			try {
				System.out.println("Entering create alert fragment.");
				Functions.buttonClickUse("com.quickoption.app:id/alert_addAlertBtn");
				Functions.countTime("com.quickoption.app:id/submit_saveChangeBtn");
				System.out.println("Entering create alert fragment succesfully");
				Functions.endFunctions();
			} catch (Exception e) {
				System.out.println("Alert fragment - Failed in getting to create alert.");
			}
		}
	}
}
