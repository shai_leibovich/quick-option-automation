package com.quickopton.test;


import java.io.File;
import java.io.IOException;
import java.net.URL;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import io.appium.java_client.android.AndroidDriver;

	
public class MainTestQuick {
    @Before
    public void test() throws IOException{
        //Setting appium details for starting test.
        File classpathRoot = new File(System.getProperty("user.dir"));
        File appDir = new File(classpathRoot,"/ContactManager");
        File app = new File(appDir,"app-debug.apk");
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("noReset", true);
		capabilities.setCapability("newCommandTimeout", "90");
        capabilities.setCapability(CapabilityType.BROWSER_NAME, "");
		capabilities.setCapability("platformName", Config.PLATFORM_NAME);
		capabilities.setCapability(CapabilityType.VERSION, "4.4");
//		capabilities.setCapability(CapabilityType.VERSION, "6.0");
        capabilities.setCapability("deviceName", Config.DEVICE_NAME);
        capabilities.setCapability("app", app.getAbsolutePath());
        capabilities.setCapability("appPackage", "com.quickoption.app");
        capabilities.setCapability("appActivity", ".activities.MainActivity");
        Config.driver = new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
    }

    @Test
	public void quickTest(){
		System.out.println("Search for Emulator succeeded");
		System.out.println("Application lunched");

		if(!Config.errorHappened)
			AccountSetting.accountSettingStage();
		if(!Config.errorHappened)
			AlertFragment.alertFragmentStage();
		if(!Config.errorHappened)
			DepositFragment.depositFragmentStage(Config.bonusUse);
		if(!Config.errorHappened)
			FinancialNewsFragment.financialNewsFragmentStage();
	}
	
	@After
	public void tearDown()throws Exception{

		
		if (Config.errorHappened){
			Functions.printErrors();
		} else {
			System.out.println("The test have completed successfully.");
		}
		Config.driver.quit();
		
	}
}
