package com.quickopton.test;

import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.appium.java_client.TouchAction;

public class Functions {

	/**
	 *  ENTERING BUTTON ID AND THE DRIVER FIND AND CLICK
	 */
	public static void buttonClickUse(String buttonId){
		Config.clickButton = Config.driver.findElement(By.id(buttonId));
		Config.clickButton.click();
	}
	/**
	 *  ENTERING BUTTON ID AND THE DRIVER FIND IT
	 *  DRIVER ENTERING THE TEXT FROM sendKey AND CLICK IT
	 */
	public static void textFieldUse(String editTextId, String sendKey){
		Config.textField = Config.driver.findElement(By.id(editTextId));
        Config.textField.clear();
		Config.textField.sendKeys(sendKey);
	}

	public static void checkNextFunctions(String field){
		if(Config.textField.isSelected()){
			System.out.println("Error, next sentence don't work after the field: "+field);
		}
	}

	// Check if notification and pop up appear after 3 and 6 seconds.
	public static void checkPush(){
		checkNotification();
		checkPopup();
	}

	// Check if notification appear after 3 seconds.
	public static void checkNotification(){
		try{
			Thread.sleep(3000);
			String errorText = Config.clickButton.getText();
//			switch (errorText){
//				case "Please try another card or contact your bank":
			
//				case ""
//			}
			System.out.println("Error message:"+errorText);
			Config.clickButton = Config.driver.findElement(By.id("com.quickoption.app:id/headerMessage_closeBtn"));
			System.out.println("Notification appear - exit notification");
			Config.clickButton.click();
		} catch (Exception e) {
			System.out.println("Notification didn't appear after 3 seconds, continue.");	
		}
	}
	
	// Check if pop up appear after 3 seconds.
	public static void checkPopup(){
		try{
			Thread.sleep(3000);
			Config.clickButton = Config.driver.findElement(By.id("com.quickoption.app:id/popupDialog_cancelBtn"));
			try {
				Config.clickButton = Config.driver.findElement(By.id("com.quickoption.app:id/popupDialog_title"));
				String popupTitle = Config.clickButton.getText();
				System.out.println("The popup have title -"+popupTitle);
			} catch (Exception e) {
				System.out.println("No title from the pop-up.");
			}
			try {
				Config.clickButton = Config.driver.findElement(By.id("com.quickoption.app:id/popupDialog_paMessage"));
				String popupTitle = Config.clickButton.getText();
				System.out.println("The popup have pa message -"+popupTitle);
			} catch (Exception e) {
				System.out.println("No pa message from the pop-up.");
			}
			try {
				Config.clickButton = Config.driver.findElement(By.id("com.quickoption.app:id/popupDialog_underImageText"));
				String popupTitle = Config.clickButton.getText();
				System.out.println("The popup have text message -"+popupTitle);
			} catch (Exception e) {
				System.out.println("No text message from the pop-up.");
			}
			System.out.println("Popup appear- exit popup");
			Config.clickButton = Config.driver.findElement(By.id("com.quickoption.app:id/popupDialog_cancelBtn"));
			Config.clickButton.click();
			
		}
		catch (Exception e) {
			System.out.println("Pop-up didn't appear after 6 seconds, continue.");	
		}
	}
	
	//FNS answer - received or continue the test
	public static boolean checkAnswer(int count, boolean skipFNS){
		try{
			Thread.sleep(2000);
			Functions.buttonClickUse("com.quickoption.app:id/layout_send_btn");
			if (!skipFNS){
				List<WebElement>fnsAnswers = Config.driver.findElements(By.name("Shai"));
				fnsAnswers.get(0).click();
				Thread.sleep(2000);
				Functions.buttonClickUse("com.quickoption.app:id/layout_send_btn");
				fnsAnswers = Config.driver.findElements(By.name("Continue"));
				fnsAnswers.get(0).click();
				return true;
			} else {
				List<WebElement>fnsAnswers = Config.driver.findElements(By.id("com.quickoption.app:id/pickerItemText"));
				fnsAnswers.get(0).click();
				System.out.println("Finish "+count+" answer.");
				return false;
			}
		}
		catch (Exception e){
			System.out.println("Finished FNS");
			return true;
		}
	}
	
	// Return to trade screen from every page using navigation menu.
	public static void returnTradeFragment(){
		System.out.println("Returning to trade screen");
		buttonClickUse("com.quickoption.app:id/menu_icon");
		try {
			Thread.sleep(1500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		new TouchAction(Config.driver).tap(245,630).release().perform();
		checkPush();
		countTime("com.quickoption.app:id/investPanel_investBox");
		endFunctions();
	}
	
	// Print *** for console
	public static void endFunctions(){
		System.out.println("***************************************************************************************");
	}

	public static void endStage(){
		System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
	}
	
	// Count time for appearing new gui
	public static void countTime(String buttonId){
		Config.startTime = System.currentTimeMillis();
		WebDriverWait wait = new WebDriverWait(Config.driver,30);
		wait.until(ExpectedConditions.elementToBeClickable(By.id(buttonId)));
		Config.endTime = System.currentTimeMillis();
		Config.totalTime = Config.endTime-Config.startTime;
		Config.totalTimeSeconds = Config.totalTime/1000;
		System.out.println("Total time to lunch next page: " + Config.totalTimeSeconds);
	}

	// Pin code pop up setting
	public static class AWTCounter extends Frame implements ActionListener {
		   private JLabel lblCount;    // Declare component Label
		   private JTextField tfCount; // Declare component TextField
		   private JButton btnCount;   // Declare component Button
		 
		   /** Constructor to setup GUI components and event handling */
		   public AWTCounter () {
		      setLayout(new FlowLayout());
		      lblCount = new JLabel("Enter pin-code");  // construct Label
		      add(lblCount);                    // "super" Frame adds Label
		      tfCount = new JTextField(10); // construct TextField
		      tfCount.setEditable(true);       // set to read-only
		      add(tfCount);                     // "super" Frame adds tfCount
		      btnCount = new JButton("Finish");   // construct Button
		      add(btnCount);                    // "super" Frame adds Button
		      btnCount.addActionListener(this);
		      setTitle("AWT Counter");  // "super" Frame sets title
		      setSize(250, 100);        // "super" Frame sets initial window size
		      setVisible(true);         // "super" Frame shows
		
		   }

		   @Override
		   public void actionPerformed(ActionEvent evt) {
		      Config.pinCode = tfCount.getText();
		      this.dispose();
		   }
		}
	
	// Using pin code pop up
	public static void getPincodePopup(){
	      AWTCounter app = new AWTCounter();
	      while(Config.pinCode == null){
	    	  try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
	      }
	   }
	
	public static void checkLevel(String wantedLevel){
		String chosenAmount;
		int depositPlace = 0;
		String chosenVIP;
		boolean silverOption = false;
		boolean goldOption = false;
		boolean platinumOption = false;
		Config.clickButton = Config.driver.findElement(By.id("com.quickoption.app:id/deposit_chosenAmount"));
		chosenAmount = Config.clickButton.getText();
		Functions.buttonClickUse("com.quicko ption.app:id/deposit_chosenAmount");
		List<WebElement>depositAvailable = Config.driver.findElements(By.id("com.quickoption.app:id/depositAmountsList_chosenAmount"));
		List<WebElement>vipPlace = Config.driver.findElements(By.id("com.quickoption.app:id/depositAmountsList_chosenAmountVipLabelText"));
		for (int i= 0 ; vipPlace.size()<i;i++){
			vipPlace.get(i).click();
			try {
				Config.clickButton = Config.driver.findElement(By.name("What is Silver VIP?"));
				silverOption = true;
			} catch (Exception e) {
				try{
					Config.clickButton = Config.driver.findElement(By.name("What is Gold VIP?"));
					goldOption = true;
				} catch (Exception fe){
					try {
						Config.clickButton = Config.driver.findElement(By.name("What is Platinum VIP?"));
						platinumOption = true;
					} catch (Exception ge){
						 chosenVIP = "none";
					}
				}
			}
		}
		for (int j =  0 ; depositAvailable.size()<j ; j++){
			if(chosenAmount.equals(depositAvailable.get(j).getText())){
				depositPlace = j;
				break;
			}
		}
		
		vipPlace.get(depositPlace).click();
		try {
			Config.clickButton = Config.driver.findElement(By.name("What is Silver VIP?"));
			chosenVIP = "silver";
		} catch (Exception e) {
			try{
				Config.clickButton = Config.driver.findElement(By.name("What is Gold VIP?"));
				chosenVIP = "gold";
			} catch (Exception fe){
				try {
					Config.clickButton = Config.driver.findElement(By.name("What is Platinum VIP?"));
					chosenVIP = "platinium";
				} catch (Exception ge){
					 chosenVIP = "none";
				}
			}
		}
		if (silverOption && wantedLevel.equals("silver")){
			if(chosenVIP.equals("silver")){
				System.out.println("Deposit set is correct");
			} else {
				System.out.println("Error, deposit set is incorrect");
				wantedLevel = "gold";
			}
		}
		else if (goldOption && wantedLevel.equals("gold")){
			System.out.println("No silver bonus option, checking for gold.");
			if (chosenVIP.equals("gold")){
				System.out.println("Deposit set is correct");
			} else {
				System.out.println("Error, deposit set is incoorect");
				wantedLevel = "platinum";
			}
		}
		else if (platinumOption && wantedLevel.equals("platinum")) {
			System.out.println("No silver and gold option, checking for platinum");
			if (chosenVIP.equals("platinum")){
				System.out.println("Deposit set is correct");
			} else {
				System.out.println("Error, deposit set is incorrect");
			}
		} 
		Config.driver.navigate().back();
		System.out.println("Returned to deposit page");
	}

	public static void addError (String errorText){
		if (!Config.errorHappened){
			Config.errorHappened = true;
		}
		Config.error[Config.errorCounter] = errorText;
		Config.errorCounter++;
	}

	public static void printErrors(){
		int i = 0;
		while (i <= Config.errorCounter && Config.errorHappened){
			System.out.println(Config.error[i]);
			i++;
		}
	}
}
